package de.memoria.app.util;

import android.annotation.SuppressLint;
import android.util.Base64;
import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class WsseToken {	
	public static final String HEADER_AUTHORIZATION = "Authorization:";
	public static final String HEADER_WSSE = "X-WSSE:";
	
	@SuppressLint("SimpleDateFormat")
	public static final SimpleDateFormat WSSE_DATETIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
	//in our case, User is an entity (just a POJO) persisted into sqlite database
	private String nonce;
	private String createdAt;
	private String digest;
    private String username;
    private String passwordHash;
    private String salt;

    /**
     *
     * @param username i.e. E-Mail
     * @param passwordHash as SHA512x5000
     */
	public WsseToken(String username, String passwordHash, String salt) {
		//we need the user object because we need his username
        this.username = username;
        this.passwordHash = passwordHash;
        this.salt = salt;
		this.createdAt = generateTimestamp();
		this.nonce = generateNonce();
		this.digest = generateDigest();
	}

	@SuppressLint("TrulyRandom")
	private String generateNonce() {
        SecureRandom random = new SecureRandom();
		byte seed[] = random.generateSeed(10);
        return new String(seed);

	}

	private String generateTimestamp() {
        WSSE_DATETIME_FORMAT.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));
		return WSSE_DATETIME_FORMAT.format(new Date());
	}

	private String generateDigest() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.nonce);
        sb.append(this.createdAt);
        sb.append(this.passwordHash);
        return Hashing.sha512x5000(this.salt, sb.toString());
	}

	public String getWsseHeader() {
		StringBuilder header = new StringBuilder();
		header.append("UsernameToken Username=\"");
		header.append(this.username);
		header.append("\", PasswordDigest=\"");
		header.append(this.digest);
		header.append("\", Nonce=\"");
		header.append(Base64.encodeToString(this.nonce.getBytes(), Base64.NO_WRAP));
		header.append("\", Created=\"");
		header.append(this.createdAt);
		header.append("\"");
		return header.toString();
	}
	
	public String getAuthorizationHeader() {
		Log.d(HEADER_AUTHORIZATION, "WSSE profile=\"UsernameToken\"");
		return  "WSSE profile=\"UsernameToken\"";
	}

    public static class Hashing {
        public static String sha512x5000(String salt, String clearPassword) {
            String hash = "";
            try {
                String salted = null;
                if(salt == null || "".equals(salt)) {
                    salted = clearPassword;
                } else {
                    salted = clearPassword + "{" + salt + "}";
                }
                MessageDigest md = MessageDigest.getInstance("SHA-512");
                byte sha[] = md.digest(salted.getBytes());
                for(int i = 1; i < 5000; i++) {
                    byte c[] = new byte[sha.length + salted.getBytes().length];
                    System.arraycopy(sha, 0, c, 0, sha.length);
                    System.arraycopy(salted.getBytes(), 0, c, sha.length, salted.getBytes().length);
                    sha = md.digest(c);
                }
                hash = new String(Base64.encode(sha,Base64.NO_WRAP));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            return hash;
        }
    }
}
