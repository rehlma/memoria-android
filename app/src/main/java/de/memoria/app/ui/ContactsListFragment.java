package de.memoria.app.ui;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.*;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.ContactsContract.Contacts;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.*;
import android.widget.*;
import de.memoria.app.BuildConfig;
import de.memoria.app.R;
import de.memoria.app.database.DatabaseHelper;
import de.memoria.app.ui.dialog.AfterDialDialog;
import de.memoria.app.ui.dialog.CalendarDialog;
import de.memoria.app.ui.dialog.DialDialog;
import de.memoria.app.ui.dialog.ReminderDialog;
import de.memoria.app.util.ImageLoader;
import de.memoria.app.util.Utils;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

public class ContactsListFragment extends ListFragment implements
        AdapterView.OnItemClickListener,
        DialDialog.DialDialogListener,
        CalendarDialog.CalendarDialogListener,
        AfterDialDialog.AfterDialDialogListener,
        ReminderDialog.ReminderDialogListener,
        LoaderManager.LoaderCallbacks<Cursor> {

    private static final String TAG = "ContactsListFragment";
    private static final String STATE_PREVIOUSLY_SELECTED_KEY = "SELECTED_ITEM";
    public static final String CALENDAR_ID = "CALENDAR_ID";
    public static final String FIRST_RUN = "FIRST_RUN";

    private ContactsAdapter mAdapter; // The main query adapter
    private ImageLoader mImageLoader; // Handles loading the contact image in a background thread
    private String mSearchTerm; // Stores the current search query term
    private boolean showAll;
    private Cursor favCursor;
    private DatabaseHelper databaseHelper;
    private Menu menu;
    private View mView;

    private int mPreviouslySelectedSearchItem = 0;

    private boolean mIsSearchResultView = false;

    public ContactsListFragment() {}

    public void setSearchQuery(String query) {
        if (TextUtils.isEmpty(query)) {
            mIsSearchResultView = false;
        } else {
            mSearchTerm = query;
            mIsSearchResultView = true;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Prüft ob ein Kalender vorhaden ist und legt in ggf. an
        SharedPreferences settings = getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);
        if (settings.getInt(CALENDAR_ID, 0) == 0) {
            setCalendar();
        }
        boolean firstRun = settings.getBoolean(FIRST_RUN, true);
        if (firstRun) {
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean(FIRST_RUN, false);
            editor.commit();
        }
        showAll = firstRun;

        databaseHelper = DatabaseHelper.getInstance(getActivity());
        databaseHelper.open(true);
        favCursor = databaseHelper.getFavorites();

        mAdapter = new ContactsAdapter(getActivity());

        if (savedInstanceState != null) {
            mSearchTerm = savedInstanceState.getString(SearchManager.QUERY);
            mPreviouslySelectedSearchItem =
                    savedInstanceState.getInt(STATE_PREVIOUSLY_SELECTED_KEY, 0);
        }

        mImageLoader = new ImageLoader(getActivity(), getListPreferredItemHeight()) {
            @Override
            protected Bitmap processBitmap(Object data) {
                return loadContactPhotoThumbnail((String) data, getImageSize());
            }
        };
        mImageLoader.setLoadingImage(R.drawable.ic_action_contact_placeholder);
        mImageLoader.addImageCache(getActivity().getSupportFragmentManager(), 0.1f);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mView != null) {
            ViewGroup parent = (ViewGroup) mView.getParent();
            if (parent != null)
                parent.removeView(mView);
        }
        try {
            mView = inflater.inflate(R.layout.contact_list_fragment, container, false);
        } catch (InflateException e) {
        /* map is already there, just return view as it is */
        }

        return mView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setHasOptionsMenu(true);

        setListAdapter(mAdapter);

        getListView().setOnItemClickListener(this);
        getListView().setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                // Pause image loader to ensure smoother scrolling when flinging
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
                    mImageLoader.setPauseWork(true);
                } else {
                    mImageLoader.setPauseWork(false);
                }
            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
            }
        });

        if (mPreviouslySelectedSearchItem == 0) {
            getLoaderManager().initLoader(ContactsQuery.QUERY_ID, null, this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mImageLoader.setPauseWork(false);
        DatabaseHelper.getInstance(getActivity()).close();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        if(!showAll) {
            // Gets the Cursor object currently bound to the ListView
            final Cursor cursor = mAdapter.getCursor();

            // Moves to the Cursor row corresponding to the ListView item that was clicked
            cursor.moveToPosition(position);

            // Creates a contact lookup Uri from contact ID and lookup_key
            Uri uri = Contacts.getLookupUri(
                    cursor.getLong(ContactsQuery.ID),
                    cursor.getString(ContactsQuery.LOOKUP_KEY));

            Intent intent = new Intent(getActivity(), DetailActivity.class);
            intent.putExtra(DetailFragment.EXTRA_CONTACT_URI, uri.toString());
            startActivity(intent);
        }
    }

    /**
     * Called when ListView selection is cleared, for example
     * when search mode is finished and the currently selected
     * contact should no longer be selected.
     */
    private void onSelectionCleared() {
        // Clears currently checked item
        getListView().clearChoices();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        // Inflate the menu items
        inflater.inflate(R.menu.contact_list_menu, menu);
        // Locate the search item
        MenuItem searchItem = menu.findItem(R.id.menu_search);
        this.menu = menu;
        setFavView(showAll);

        if (mIsSearchResultView) {
            searchItem.setVisible(false);
        }

        // Retrieves the system search manager service
        final SearchManager searchManager =
                (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        // Retrieves the SearchView from the search menu item
        final SearchView searchView = (SearchView) searchItem.getActionView();

        // Assign searchable info to SearchView
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getActivity().getComponentName()));

        // Set listeners for SearchView
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String queryText) {
                // Nothing needs to happen when the user submits the search string
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // Called when the action bar search text has changed.  Updates
                // the search filter, and restarts the loader to do a new query
                // using the new search string.
                String newFilter = !TextUtils.isEmpty(newText) ? newText : null;

                // Don't do anything if the filter is empty
                if (mSearchTerm == null && newFilter == null) {
                    return true;
                }

                // Don't do anything if the new filter is the same as the current filter
                if (mSearchTerm != null && mSearchTerm.equals(newFilter)) {
                    return true;
                }

                // Updates current filter to new filter
                mSearchTerm = newFilter;

                getLoaderManager().restartLoader(
                        ContactsQuery.QUERY_ID, null, ContactsListFragment.this);
                return true;
            }
        });

        // This listener added in ICS
        searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                if (!TextUtils.isEmpty(mSearchTerm)) {
                    onSelectionCleared();
                }
                mSearchTerm = null;
                getLoaderManager().restartLoader(
                        ContactsQuery.QUERY_ID, null, ContactsListFragment.this);
                return true;
            }
        });

        if (mSearchTerm != null) {
            final String savedSearchTerm = mSearchTerm;

            // Expands the search menu item
            searchItem.expandActionView();

            // Sets the SearchView to the previous search string
            searchView.setQuery(savedSearchTerm, false);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (!TextUtils.isEmpty(mSearchTerm)) {
            // Saves the current search string
            outState.putString(SearchManager.QUERY, mSearchTerm);

            // Saves the currently selected contact
            outState.putInt(STATE_PREVIOUSLY_SELECTED_KEY, getListView().getCheckedItemPosition());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add_contact:
                Intent addContactIntent = new Intent(Intent.ACTION_INSERT, Contacts.CONTENT_URI);
                startActivity(addContactIntent);
                break;
            case R.id.menu_reminder:
                Intent reminderIntent = new Intent(getActivity(), ReminderActivity.class);
                startActivity(reminderIntent);
                break;
            case R.id.menu_favorite:
                setFavView(!showAll);
                break;
            case R.id.menu_change_calendar:
                setCalendar();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        // If this is the loader for finding contacts in the Contacts Provider
        // (the only one supported)
        if (id == ContactsQuery.QUERY_ID) {
            Uri contentUri;

            if (mSearchTerm == null) {
                // Since there's no search string, use the content URI that searches the entire
                // Contacts table
                contentUri = ContactsQuery.CONTENT_URI;
            } else {
                // Since there's a search string, use the special content Uri that searches the
                // Contacts table. The URI consists of a base Uri and the search string.
                contentUri =
                        Uri.withAppendedPath(ContactsQuery.FILTER_URI, Uri.encode(mSearchTerm));
            }

            // Returns a new CursorLoader for querying the Contacts table. No arguments are used
            // for the selection clause. The search string is either encoded onto the content URI,
            // or no contacts search string is used. The other search criteria are constants. See
            // the ContactsQuery interface.
            return new CursorLoader(getActivity(),
                    contentUri,
                    ContactsQuery.PROJECTION,
                    buildWhere(),
                    null,
                    ContactsQuery.SORT_ORDER);
        }

        Log.e(TAG, "onCreateLoader - incorrect ID provided (" + id + ")");
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // This swaps the new cursor into the adapter.
        if (loader.getId() == ContactsQuery.QUERY_ID) {
            mAdapter.swapCursor(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (loader.getId() == ContactsQuery.QUERY_ID) {
            mAdapter.swapCursor(null);
        }
    }

    private int getListPreferredItemHeight() {
        final TypedValue typedValue = new TypedValue();

        // Resolve list item preferred height theme attribute into typedValue
        getActivity().getTheme().resolveAttribute(
                android.R.attr.listPreferredItemHeight, typedValue, true);

        // Create a new DisplayMetrics object
        final DisplayMetrics metrics = new android.util.DisplayMetrics();

        // Populate the DisplayMetrics
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        // Return theme value based on DisplayMetrics
        return (int) typedValue.getDimension(metrics);
    }

    private Bitmap loadContactPhotoThumbnail(String photoData, int imageSize) {
        if (!isAdded() || getActivity() == null) {
            return null;
        }
        AssetFileDescriptor afd = null;

        try {
            Uri thumbUri = Uri.parse(photoData);

            // Retrieves a file descriptor from the Contacts Provider. To learn more about this
            // feature, read the reference documentation for
            // ContentResolver#openAssetFileDescriptor.
            afd = getActivity().getContentResolver().openAssetFileDescriptor(thumbUri, "r");

            // Gets a FileDescriptor from the AssetFileDescriptor. A BitmapFactory object can
            // decode the contents of a file pointed to by a FileDescriptor into a Bitmap.
            FileDescriptor fileDescriptor = afd.getFileDescriptor();

            if (fileDescriptor != null) {
                // Decodes a Bitmap from the image pointed to by the FileDescriptor, and scales it
                // to the specified width and height
                return ImageLoader.decodeSampledBitmapFromDescriptor(
                        fileDescriptor, imageSize, imageSize);
            }
        } catch (FileNotFoundException e) {
            // If the file pointed to by the thumbnail URI doesn't exist, or the file can't be
            // opened in "read" mode, ContentResolver.openAssetFileDescriptor throws a
            // FileNotFoundException.
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Contact photo thumbnail not found for contact " + photoData + ": " + e.toString());
            }
        } finally {
            // If an AssetFileDescriptor was returned, try to close it
            if (afd != null) {
                try {
                    afd.close();
                } catch (IOException e) {
                    // Closing a file descriptor might cause an IOException if the file is
                    // already closed. Nothing extra is needed to handle this.
                }
            }
        }

        // If the decoding failed, returns null
        return null;
    }

    @Override
    public void onDialDialogCallback(DialDialog dialDialog, String contactID, String contactName, String contactUri) {
        dialDialog.dismiss();
        AfterDialDialog afterDialDialog = new AfterDialDialog();
        Bundle bundle = new Bundle();
        bundle.putString(AfterDialDialog.CONTACT_ID, contactID);
        bundle.putString(AfterDialDialog.CONTACT_NAME, contactName);
        bundle.putString(AfterDialDialog.CONTACT_URI, contactUri);
        afterDialDialog.setArguments(bundle);
        afterDialDialog.setTargetFragment(ContactsListFragment.this, 0);
        afterDialDialog.show(getFragmentManager(), "afterDialDialog");
    }

    @Override
    public void onReminderDialogCallback(ReminderDialog reminderDialog, Bundle bundle) {
//        DatabaseHelper databaseHelper = DatabaseHelper.getInstance(getActivity().getApplicationContext());
//        databaseHelper.open();
        databaseHelper.insertReminder(
                bundle.getString(ReminderDialog.REMINDER_TITLE),
                bundle.getString(ReminderDialog.REMINDER_DATE)
        );
        reminderDialog.dismiss();
    }

    @Override
    public void onCalendarDialogCallback(CalendarDialog CalendarDialog, Bundle bundle) {
//        DatabaseHelper databaseHelper = DatabaseHelper.getInstance(getActivity().getApplicationContext());
//        databaseHelper.open();

        SharedPreferences settings = getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);
        int calendarId = settings.getInt(CALENDAR_ID, 0);
        ContentResolver cr = getActivity().getContentResolver();

        Calendar beginTime = Calendar.getInstance();
        beginTime.setTimeInMillis(bundle.getLong(CalendarDialog.CALENDAR_DATE));

        // Set event values
        ContentValues eventValues = new ContentValues();
        eventValues.put(CalendarContract.Events.TITLE, bundle.getString(CalendarDialog.CALENDAR_TITLE));
        eventValues.put(CalendarContract.Events.DTSTART, beginTime.getTimeInMillis());
        eventValues.put(CalendarContract.Events.DTEND, beginTime.getTimeInMillis());
        eventValues.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().getID());
        eventValues.put(CalendarContract.Events.CALENDAR_ID, calendarId);
        Uri insertUri = cr.insert(CalendarContract.Events.CONTENT_URI, eventValues);

        int eventID = Integer.parseInt(insertUri.getLastPathSegment());

        // Add Contact to event;
        databaseHelper.insertEvents(bundle.getString(CalendarDialog.CONTACT_ID), eventID);

        // Add reminder to event
        ContentValues reminderValues = new ContentValues();
        reminderValues.put(CalendarContract.Reminders.MINUTES, 15);
        reminderValues.put(CalendarContract.Reminders.EVENT_ID, eventID);
        reminderValues.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
        cr.insert(CalendarContract.Reminders.CONTENT_URI, reminderValues);

        Uri uri = Uri.parse(bundle.getString(CalendarDialog.CONTACT_URI));
        if (uri != null) {
            Intent intent = new Intent(getActivity(), DetailActivity.class);
            intent.putExtra(DetailFragment.EXTRA_CONTACT_URI, uri.toString());
            intent.putExtra(DetailFragment.SELECTED_MODE, DetailFragment.MODE_CALENDAR);
            startActivity(intent);
        }

        // Add reminder for event
        Calendar reminderDate = beginTime;
        reminderDate.add(Calendar.DAY_OF_MONTH, 1);
        String date = (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")).format(reminderDate.getTimeInMillis());
        databaseHelper.insertReminder(
                bundle.getString(CalendarDialog.CALENDAR_TITLE),
                date
        );

        // Open Event
        Uri editUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, eventID);
        Intent calendarIntent = new Intent(Intent.ACTION_VIEW)
                .setData(editUri);
        startActivity(calendarIntent);
    }

    @Override
    public void onAfterDialDialogCallback(AfterDialDialog afterDialDialog, Bundle bundle) {
        afterDialDialog.dismiss();
        if (bundle.getString(AfterDialDialog.ACTION).equals(AfterDialDialog.REMINDER)) {
            ReminderDialog reminderDialog = new ReminderDialog();
            bundle.putString(ReminderDialog.CONTACT_NAME, bundle.getString(AfterDialDialog.CONTACT_NAME));
            reminderDialog.setArguments(bundle);
            reminderDialog.setTargetFragment(ContactsListFragment.this, 0);
            reminderDialog.show(getFragmentManager(), "reminderDialog");
        }
        else if (bundle.getString(AfterDialDialog.ACTION).equals(AfterDialDialog.APPOINTMENT)) {
            CalendarDialog calendarDialog = new CalendarDialog();
            bundle.putString(CalendarDialog.CONTACT_NAME, bundle.getString(AfterDialDialog.CONTACT_NAME));
            bundle.putString(CalendarDialog.CONTACT_URI, bundle.getString(AfterDialDialog.CONTACT_URI));
            bundle.putString(CalendarDialog.CONTACT_ID, bundle.getString(AfterDialDialog.CONTACT_ID));
            calendarDialog.setArguments(bundle);
            calendarDialog.setTargetFragment(ContactsListFragment.this, 0);
            calendarDialog.show(getFragmentManager(), "calendarDialog");
        }
    }

    private void setCalendar() {
        HashMap<Integer, String> calenders = Utils.Calendar.isCalendarAvailable(getActivity());
        if (calenders.isEmpty()) {
            Utils.Calendar.addCalendar(getActivity(), getResources().getString(R.string.calendar_name));
            calenders = Utils.Calendar.isCalendarAvailable(getActivity());
        }
        final Integer[] keys = calenders.keySet().toArray(new Integer[calenders.size()]);
        String[] values = calenders.values().toArray(new String[calenders.size()]);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle(R.string.menu_contact_change_calendar);
        alertDialog.setSingleChoiceItems(values, 0, null);
        alertDialog.setPositiveButton(R.string.dialog_generel_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                int selectedPosition = ((AlertDialog) dialogInterface).getListView().getCheckedItemPosition();

                SharedPreferences settings = getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putInt(CALENDAR_ID, keys[selectedPosition]);
                editor.commit();
            }
        });
        alertDialog.show();
    }

    /**
     * This is a subclass of CursorAdapter that supports binding Cursor columns to a view layout.
     * If those items are part of search results, the search string is marked by highlighting the
     * query text. An {@link AlphabetIndexer} is used to allow quicker navigation up and down the
     * ListView.
     */
    private class ContactsAdapter extends CursorAdapter implements SectionIndexer {
        private AlphabetIndexer mAlphabetIndexer; // Stores the AlphabetIndexer instance
        private TextAppearanceSpan highlightTextSpan; // Stores the highlight text appearance style

        public ContactsAdapter(Context context) {
            super(context, null, 0);

            final String alphabet = context.getString(R.string.alphabet);

            mAlphabetIndexer = new AlphabetIndexer(null, ContactsQuery.SORT_KEY, alphabet);
            highlightTextSpan = new TextAppearanceSpan(getActivity(), R.style.searchTextHiglight);
        }

        /**
         * Identifies the start of the search string in the display name column of a Cursor row.
         * E.g. If displayName was "Adam" and search query (mSearchTerm) was "da" this would
         * return 1.
         *
         * @param displayName The contact display name.
         * @return The starting position of the search string in the display name, 0-based. The
         * method returns -1 if the string is not found in the display name, or if the search
         * string is empty or null.
         */
        private int indexOfSearchQuery(String displayName) {
            if (!TextUtils.isEmpty(mSearchTerm)) {
                return displayName.toLowerCase(Locale.getDefault()).indexOf(
                        mSearchTerm.toLowerCase(Locale.getDefault()));
            }
            return -1;
        }

        /**
         * Overrides newView() to inflate the list item views.
         */
        @Override
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
            View itemLayout = inflater.inflate(R.layout.contact_list_item, viewGroup, false);
            return itemLayout;
        }

        /**
         * Binds data from the Cursor to the provided view.
         */
        @Override
        public void bindView(View view, Context context, final Cursor cursor) {
            final String photoUri = cursor.getString(ContactsQuery.PHOTO_THUMBNAIL_DATA);

            final String displayName = cursor.getString(ContactsQuery.DISPLAY_NAME);

            final long contactID = cursor.getLong(ContactsQuery.ID);

            final int startIndex = indexOfSearchQuery(displayName);

            TextView contactListItemName = (TextView) view.findViewById(R.id.contactListItemName);
            ImageView contactListItemIcon = (ImageView) view.findViewById(R.id.contactListItemIcon);
            ImageButton contactListItemCallButton = (ImageButton) view.findViewById(R.id.contactListItemCallButton);
            contactListItemCallButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Uri uri = Contacts.getLookupUri(
                            cursor.getLong(ContactsQuery.ID),
                            cursor.getString(ContactsQuery.LOOKUP_KEY));

                    Bundle bundle = new Bundle();
                    bundle.putString(DialDialog.CONTACT_ID, contactID+"");
                    bundle.putString(DialDialog.CONTACT_NAME, displayName);
                    bundle.putString(DialDialog.CONTACT_URI, uri.toString());

                    DialDialog dialDialog = new DialDialog();
                    dialDialog.setTargetFragment(ContactsListFragment.this, 0);
                    dialDialog.setArguments(bundle);
                    dialDialog.show(getFragmentManager(),"dialDialog");
                }
            });
            final ImageButton contactListItemFavButton = (ImageButton) view.findViewById(R.id.contactListItemFavButton);
            contactListItemFavButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    databaseHelper.open(true);
                    if (isContactFavorite(contactID)) {
                        databaseHelper.deleteFavorite(contactID);
                        databaseHelper.deleteEventsByContactId(String.valueOf(contactID));
                        contactListItemFavButton.setImageResource(R.drawable.ic_action_favorite_remove_green);
                    } else {
                        databaseHelper.insertFavorite(contactID);
                        contactListItemFavButton.setImageResource(R.drawable.ic_action_favorite_add_green);
                    }
                    favCursor = databaseHelper.getFavorites();
                }
            });

            if(isContactFavorite(contactID)) contactListItemFavButton.setImageResource(R.drawable.ic_action_favorite_add_green);
            else contactListItemFavButton.setImageResource(R.drawable.ic_action_favorite_remove_green);
            if(showAll) {
                contactListItemCallButton.setVisibility(View.GONE);
                contactListItemFavButton.setVisibility(View.VISIBLE);
            } else {
                contactListItemCallButton.setVisibility(View.VISIBLE);
                contactListItemFavButton.setVisibility(View.GONE);
            }

            if (startIndex == -1) {
                contactListItemName.setText(displayName);
            } else {
                final SpannableString highlightedName = new SpannableString(displayName);
                highlightedName.setSpan(highlightTextSpan, startIndex, startIndex + mSearchTerm.length(), 0);
                contactListItemName.setText(highlightedName);
            }
            // Loads the thumbnail image pointed to by photoUri into the ImageView in a
            // background worker thread
            mImageLoader.loadImage(photoUri, contactListItemIcon);
        }

        /**
         * Overrides swapCursor to move the new Cursor into the AlphabetIndex as well as the
         * CursorAdapter.
         */
        @Override
        public Cursor swapCursor(Cursor newCursor) {
            // Update the AlphabetIndexer with new cursor as well
            mAlphabetIndexer.setCursor(newCursor);
            return super.swapCursor(newCursor);
        }

        /**
         * An override of getCount that simplifies accessing the Cursor. If the Cursor is null,
         * getCount returns zero. As a result, no test for Cursor == null is needed.
         */
        @Override
        public int getCount() {
            if (getCursor() == null) {
                return 0;
            }
            return super.getCount();
        }

        /**
         * Defines the SectionIndexer.getSections() interface.
         */
        @Override
        public Object[] getSections() {
            return mAlphabetIndexer.getSections();
        }

        /**
         * Defines the SectionIndexer.getPositionForSection() interface.
         */
        @Override
        public int getPositionForSection(int i) {
            if (getCursor() == null) {
                return 0;
            }
            return mAlphabetIndexer.getPositionForSection(i);
        }

        /**
         * Defines the SectionIndexer.getSectionForPosition() interface.
         */
        @Override
        public int getSectionForPosition(int i) {
            if (getCursor() == null) {
                return 0;
            }
            return mAlphabetIndexer.getSectionForPosition(i);
        }
    }

    /**
     * This interface defines constants for the Cursor and CursorLoader, based on constants defined
     * in the {@link android.provider.ContactsContract.Contacts} class.
     */
    public interface ContactsQuery {

        // An identifier for the loader
        final static int QUERY_ID = 1;

        // A content URI for the Contacts table
        final static Uri CONTENT_URI = Contacts.CONTENT_URI;

        // The search/filter query Uri
        final static Uri FILTER_URI = Contacts.CONTENT_FILTER_URI;

        final static String SELECTION =
                Contacts.DISPLAY_NAME_PRIMARY +
                        "<>''" +
                        " AND " + Contacts.IN_VISIBLE_GROUP + "=1";

        final static String SORT_ORDER = Contacts.SORT_KEY_PRIMARY;

        final static String[] PROJECTION = {
                Contacts._ID,
                Contacts.LOOKUP_KEY,
                Contacts.DISPLAY_NAME_PRIMARY,
                Contacts.PHOTO_THUMBNAIL_URI,
                SORT_ORDER,
        };

        final static int ID = 0;
        final static int LOOKUP_KEY = 1;
        final static int DISPLAY_NAME = 2;
        final static int PHOTO_THUMBNAIL_DATA = 3;
        final static int SORT_KEY = 4;
    }

    private String buildWhere() {
        StringBuilder sb = new StringBuilder();

        sb.append(ContactsQuery.SELECTION);

        if(!showAll) {
            sb.append(" AND " + Contacts._ID + " IN (");
            if(favCursor != null && favCursor.moveToFirst()) {
                do {
                    if (favCursor.isLast()) sb.append(favCursor.getLong(ContactsQuery.ID));
                    else sb.append(favCursor.getLong(ContactsQuery.ID) + ",");
                }
                while (favCursor.moveToNext());
            }
            sb.append(" ) ");
        }

        return sb.toString();
    }

    private boolean isContactFavorite(long id) {
        if(favCursor.moveToFirst()) {
            do {
                if (id == favCursor.getLong(ContactsQuery.ID)) return true;
            }
            while (favCursor.moveToNext());
        }
        return false;
    }

    private void setFavView(boolean showAll) {
        if(menu != null) {
            MenuItem item = menu.findItem(R.id.menu_favorite);
            if (!showAll) {
                this.showAll = false;
                item.setIcon(R.drawable.ic_action_favorite);
                ((TextView) getListView().getEmptyView()).setText(R.string.no_favorites);
            } else {
                this.showAll = true;
                item.setIcon(R.drawable.ic_action_favorite_edit);
                ((TextView) getListView().getEmptyView()).setText(R.string.no_contacts);
            }
            getLoaderManager().restartLoader(ContactsQuery.QUERY_ID, null, this);
        }
    }
}
