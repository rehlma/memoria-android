package de.memoria.app.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.*;
import de.memoria.app.R;

/**
 * Created by maikel on 30.10.14.
 */

public class AfterDialDialog extends DialogFragment {

    public static final String CONTACT_ID = "_CONTACT_ID";
    public static final String CONTACT_NAME = "_CONTACT_NAME";
    public static final String CONTACT_URI = "_CONTACT_URI";
    public static final String ACTION = "_ACTION";
    public static final String REMINDER = "_REMINDER";
    public static final String APPOINTMENT = "_APPOINTMENT";


    public interface AfterDialDialogListener {
        public void onAfterDialDialogCallback(AfterDialDialog afterDialDialog, Bundle bundle);
    }

    public void setAfterDialDialogListener(AfterDialDialogListener callback) {
        this.callback = callback;
    }

    AfterDialDialogListener callback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.memoriaDialog);
        try {
            if(callback == null)
                callback = (AfterDialDialogListener) getTargetFragment();
        } catch (Exception e) {
            throw new ClassCastException("Calling Fragment must implement onAfterDialDialogCallback");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getActivity();

        final Bundle putBundle = new Bundle();
        if(getArguments() != null) {
            Bundle getBundle = new Bundle();
            getBundle.putAll(getArguments());
            if (getBundle.containsKey(CONTACT_ID) && getBundle.containsKey(CONTACT_NAME)) {
                putBundle.putString(CONTACT_ID, getBundle.getString(CONTACT_ID));
                putBundle.putString(CONTACT_NAME, getBundle.getString(CONTACT_NAME));
                putBundle.putString(CONTACT_URI, getBundle.getString(CONTACT_URI));
            }
        }

        String[] values = new String[] {
                context.getResources().getString(R.string.menu_reminder_add),
                context.getResources().getString(R.string.menu_detail_add_calendar)
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);

        ListView listView = new ListView(context);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        putBundle.putString(ACTION, REMINDER);
                        break;
                    case 1:
                        putBundle.putString(ACTION, APPOINTMENT);
                        break;
                }
                callback.onAfterDialDialogCallback(AfterDialDialog.this, putBundle);
            }
        });

        final TextView calendar = new TextView(getActivity());
        calendar.setText(R.string.menu_detail_add_calendar);
        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                putBundle.putString(ACTION, APPOINTMENT);
                callback.onAfterDialDialogCallback(AfterDialDialog.this, putBundle);
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(listView);
        builder.setTitle(R.string.dialog_after_dial_title);

        builder.setNegativeButton(R.string.dialog_generel_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                    AfterDialDialog.this.getDialog().cancel();
            }
        });
        return builder.create();
    }
}
