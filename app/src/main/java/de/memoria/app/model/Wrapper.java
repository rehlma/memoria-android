package de.memoria.app.model;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

/**
 * Created by maikel on 30.03.15.
 */
public class Wrapper {

    @SerializedName("state")
    private int state;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private JsonElement data;

    public int getState() {
        return state;
    }

    public String getMessage() {
        return message;
    }

    public JsonElement getData() {
        return data;
    }
}
