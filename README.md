MeMoria hilft Ihnen bei Ihrem vertrieblichen Abläufen. 
MeMoria eignet sich optimal für Selbstständige, Unternehmer und Verkäufer.
Mit MeMoria können Sie kinderleicht Ihre Kontakte verwalten. 
Nach der Kundenakquise automatisch Terminbestätigungen per SMS, Whats App, Email oder Facebook versenden.

MeMoria erinnert fragt einem Tag nach dem Termin, ob dieser auch stattgefunden hat.
So kann kein Neukunde mehr vergessen werden.

Nach dem Telefonat mit Ihren Kunden / Interessenten Notizen bequem und einfach eintragen.

## FREUNDLICHE ERINNERUNGEN ##
Sie sollten Ihren Kunden in zwei Wochen nochmal kontaktieren und haben es leider vergessen? Keine Sorge, MeMoria hilft Ihnen und hat für viele Situationen die passenden Erinnerungsfunktionen.

## TERMIN-MANAGEMENT ##
MeMoria fragt nach jedem Telefonat, wie Sie mit Ihrem Kunden verblieben sind.

## EINFACHE KUNDENPFLEGE ##
Schnelle, übersichtliche und einheitliche Kundenverwaltung. Notiz- u. Erinnerungsfelder für jeden Kunden möglich.

## TERMIN MANAGEMENT ##
Automatische Terminbestätigung per Email oder SMS möglich.
MeMoria fragt einen Tag nach dem vereinbarten Termin, ob dieser stattgefunden hat.

## EINFACHE BEDIENUNG ##
MeMoria beeindruckt durch kinderleichte Bedienung und praxisnahe Entwicklung.

## KUNDENPFLEGE ##
Übersichtliche Notiz- und Erinnerungsfelder für jeden Kunden.


# Ihre Vorteile im Überblick #
* MeMoria überzeugt durch automatische Terminerstellung und Bestätigungsfunktionen.
* Effektiv Kunden zufriedenstellen. Im Notizfeld von jedem Kunden tragen Sie die Details Ihres Gesprächs bei Bedarf ein.
* Kein Kunde kann mehr durch ein cleveres Erinnerungs- und Wiedervorlagesystem vergessen werden.
* MeMoria eignet sich für die Neukunden Akquise oder Bestandsaktionen.


Testen Sie MeMoria 7 Tage kostenlos. 
Um MeMoria testen zu können müssen Sie sich ein Benutzerkonto einrichten. (Link zur Anmeldung in der App vorhanden)

Um MeMoria nach den 7 Tagen nutzen zu können, müssen sie erneut eine Zahlung über Paypal ausführen. 
Erforderliche Links auch in MeMoria oder unter [www.MeMoria-App.de](https://www.memoria-app.de)

Wenn Sie MeMoria nach den 7 Tagen weiter nutzen möchten, kostet es lediglich 9,99€ / Monat um Ihre Kunden perfekt und einfach zu verwalten.

MeMoria - die Arbeitserleichterung reloaded.

![login](https://bitbucket.org/repo/Epgn5K/images/1699665111-unnamed.png)
![overview](https://bitbucket.org/repo/Epgn5K/images/390220493-unnamed-3.png)
![search](https://bitbucket.org/repo/Epgn5K/images/2179587480-unnamed-2.png)
![appointment](https://bitbucket.org/repo/Epgn5K/images/947754604-unnamed-6.png)
![send_sms](https://bitbucket.org/repo/Epgn5K/images/1669212127-unnamed-4.png)
![reminder](https://bitbucket.org/repo/Epgn5K/images/831461841-unnamed-5.png)